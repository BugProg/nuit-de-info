from appwrite.client import Client
from openai import OpenAI
import json

client = OpenAI(
    api_key="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",
)


# This is your Appwrite function
# It's executed each time we get a request
def main(context):
    lang = json.loads(context.req.body)["lang"]
    jsonSS =  str(json.loads(context.req.body)["json"])
    # print(prompt)
    # context.log(prompt)

    chat_completion = client.chat.completions.create(
        messages=[
            {
                "role": "user",
                "content": "Tu es une API de traduction, je te donne les textes à traduire sous forme de json, et tu me les renvoie sous le même format json et uniquement sous la forme de json sans rien dire d'autre, traduis en "+ lang +" le fichier:" + jsonSS,
            }
        ],
        model="gpt-3.5-turbo",
    )

    content = chat_completion.choices[0].message.content
    print(content)

    return context.res.json(
        {
            "color": content,
        }
    )
