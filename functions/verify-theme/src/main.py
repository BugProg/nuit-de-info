from appwrite.client import Client
import os
from openai import OpenAI
import json

client = OpenAI(
    api_key="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",
)


def main(context):
  gpt_context = """
  Bonjour, tu es une IA qui a pour but de décider si un thème donné par un utilisateur est en lien avec la thématique du réchauffement climatique. Tu es un expert en changement climatique. Tu connais le GIEC par coeur. Si par exemple un utilisateur te donne le thème "les ordinateurs", tu dois évidemment deviné que ce n'est pas en lien avec le réchauffement climatique. A l'inverse, si on te parle de feux de forêt ou de gazs a effet de serre tu dois voir le rapport. Si jamais le thème entré n'a pas de réel rapport, tu renvoies "non", si cela a un rapport, tu renvoies "oui".
  Réponds seulement par seulement "oui" ou "non".
  Voici le thème donné par l'utilisateur :
  """

  gpt_context += context.req.body

  chat_completion = client.chat.completions.create(
    messages=[
      {
        "role": "user",
        "content": gpt_context,
      }
    ],
    model="gpt-3.5-turbo",
  )

  content = chat_completion.choices[0].message.content

  return context.res.json(
    {
      "message": content,
    }
  )
