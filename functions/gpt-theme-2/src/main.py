from appwrite.client import Client
from openai import OpenAI
import json

client = OpenAI(
    api_key="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",
)


# This is your Appwrite function
# It's executed each time we get a request
def main(context):
    prompt = json.loads(context.req.body)["prompt"]
    # print(prompt)
    context.log(prompt)

    chat_completion = client.chat.completions.create(
        messages=[
            {
                "role": "user",
                "content": "Give a random color in hexa format without extra sentence on the theme of and not saturated: " + prompt,
            }
        ],
        model="gpt-3.5-turbo",
    )

    content = chat_completion.choices[0].message.content
    print(content)

    return context.res.json(
        {
            "color": content,
        }
    )
