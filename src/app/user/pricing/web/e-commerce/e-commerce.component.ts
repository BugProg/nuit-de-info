import {ContactModalComponent} from '../../../shared/components/contact/contact-modal/contact-modal.component';
import {MatDialog} from '@angular/material/dialog';
import {Component} from '@angular/core';

@Component({
  selector: 'app-e-pricing-commerce',
  templateUrl: './e-commerce.component.html',
  styleUrls: ['./e-commerce.component.css']
})
export class ECommerceComponent {
  constructor(private dialog: MatDialog) {
  }

  openContactDialog() {
    this.dialog.open(ContactModalComponent, {
      width: '40em',
    });
  }
}
