/*
 * @copyright Copyright (c) 2023, DERACHE Adrien a.d44@tuta.io
 */

import { TranslateService } from '@ngx-translate/core';
import { Injectable } from '@angular/core';
import {BehaviorSubject} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class LanguageService {
  language: BehaviorSubject<object> = new BehaviorSubject<object>({});

  translations: BehaviorSubject<object> = new BehaviorSubject<object>({
    "subtitle" : "Vu par l'IA",
  "firstitem": "Le nucléaire",
  "firstitem1": " Ami ou ennemi ?",
  "seconditem" : "Les transports",
  "seconditem2" : "Se déplacer de manière éco-responsable",
  "thirditem" : "Alalala",
  "thirditem2" : "Bababab",
  "solutions" : "Des problèmes, des solutions",
  "solutions1" : "Comme le rapporte les membres du GIEC, le changement climatique entraîne des effets néfastes d'ores et déjà observables, tels que des phénomènes météorologiques extrêmes plus fréquents comme des feux de forêts, des ouragans ou encore d'autres changements comme la fonte des glaces ou des perturbations massives dans l'agriculture. Cependant, il n'est jamais trop tard pour agir ! Des solutions innovantes émergent à différentes échelles, de l'adoption d'énergies renouvelables à la reforestation ou à l'effort individuel. Collaborer pour construire un avenir durable et résilient face à ces défis environnementaux est essentiel.",
  "quiz" : "Le quizz climatique, INTOX ou INFO",
  "shift" : "Le Shift Project",
  "shift1" : "Le Shift Project, initié par Jean-Marc Jancovici, ex membre du GIEC,  vise à promouvoir une transition vers une économie à faible émission de carbone. Ce think tank se concentre sur la sensibilisation et le développement de propositions concrètes pour décarboner l'économie,contribuant ainsi à la lutte contre le changement climatique. Il agit notamment dans le domaine de l'agriculture qui est particulièrement émettrice de gazs à effet de serre, mais intervient également dans les entreprises pour les inciter et les aider à se décarboner.",
  "nuclear" : "L'Énergie Nucléaire :       \n Ami ou Ennemi ?",
  "the" : "Le",
  "climatic" : "climatique",
  "warming": "Réchauffement",
  "changement": "Changement",
  "disruption": "Dérèglement",
  })


  constructor(
    private translate: TranslateService,
  ) {}

  getTranslation(key: string)
  {
    // @ts-ignore
    return this.translations.value[key];
  }

  setNewKey(key: string, value: string) {
    // @ts-ignore
    this.translations.value[key] = value;
  }

  setNewTranslations(translations: any) {
    this.translations.next(translations)
  }

  /**
   * Initialize languages at application startup.
   */
  setInitialAppLanguage() {
      const language =
        this.translate.getBrowserLang() === undefined
          ? this.translate.getBrowserLang()!
          : 'fr';
      this.translate.setDefaultLang('fr');
      this.setLanguage('fr');
  }

  getLanguage() {
    return this.translate.currentLang;
  }

  /**
   * Change app language.
   * @param lng the new language.
   */
  setLanguage(lng: string) {
      this.translate.use(lng);
  }
}
