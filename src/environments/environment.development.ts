export const environment = {
  production: false,
  serverEndpoint: 'https://cloud.appwrite.io/v1',
  projectId: '6571f92e64fd5717d15f',
  formsDatabaseId: '642c2c67eb4100b9189e',
  contactCollectionId: '642c2c7e6cc45fe85697',
  reviewCollectionId: '642c2f50e2ac79094750',
  contactFunctionId: '655792758ddf3f969185',
  avatarBucketId: '64332c03202fda80154a',
  assistantFunctionId: '65723d72da84553c757c',
  themeFunction: '65724177e1ccdf649925',
  verifyThemeFunction: '65727b15e118e6553585',
};
